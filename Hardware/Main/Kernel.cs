﻿using System;
using Sys = Cosmos.System;
using Display;

namespace Main
{
    public class Kernel : Sys.Kernel
    {
        DisplayDriver display;
        protected override void BeforeRun()
        {
            Console.WriteLine("Left click the color to change the screen and right click to reset");
            display = new DisplayDriver();
        }

        protected override void Run()
        {
            display.init();
            
        }
    }
}
