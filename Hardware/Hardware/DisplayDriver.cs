﻿using Cosmos.HAL;

namespace Display
{
    public class DisplayDriver
    {
        protected VGAScreen screen;
        private int width, height, mouseX, mouseY, mouseX2, mouseY2;
        public static Mouse m;
        private int currentcolor = 7;
        private int clicker = 0;

        public DisplayDriver()
        {
            screen = new VGAScreen();
            screen.SetGraphicsMode(VGAScreen.ScreenSize.Size320x200, VGAScreen.ColorDepth.BitDepth8);
            m = new Mouse();
            m.X = 16;
            m.Y = 5;
            m.Initialize(320,200);
            

            width = screen.PixelWidth;
            height = screen.PixelHeight;

            screenfill(7);
            mouseX2 = mouseX;
            mouseY2 = mouseY;
        }

        public void screenfill(int color)
        {
            clear(color);
            DrawFilledRectangle(10, 0, 1, height, 100);
            DrawFilledRectangle(0, 0, 10, 50, 3);
            DrawFilledRectangle(0, 50, 10, 50, 2);
            DrawFilledRectangle(0, 100, 10, 50, 55);
            DrawFilledRectangle(0, 150, 10, 50, 4);
            //DrawFilledRectangle(11, 0, width - 11, height, color);

            drawmouse();
        }

        public void drawmouse()
        {
            mouseX = m.X;
            mouseY = m.Y;
            screen.SetPixel((uint)m.X, (uint)m.Y, 40);
            screen.SetPixel((uint)m.X + 1, (uint)m.Y, 40);
            screen.SetPixel((uint)m.X + 2, (uint)m.Y, 40);
            screen.SetPixel((uint)m.X, (uint)m.Y + 1, 40);
            screen.SetPixel((uint)m.X, (uint)m.Y + 2, 40);
            screen.SetPixel((uint)m.X + 1, (uint)m.Y + 1, 40);
            screen.SetPixel((uint)m.X + 2, (uint)m.Y + 2, 40);
        }

        public void init()
        {
            if (m.Buttons == Mouse.MouseState.Left)
            {
                if(m.X<= 11)
                {
                    if (m.Y <= 50 && clicker != 1)
                    {
                        screenfill(3);
                        currentcolor = 3;
                        clicker = 1;
                    }
                    else if (m.Y >= 51 && m.Y <= 100 && clicker != 2)
                    {
                        screenfill(2);
                        currentcolor = 2;
                        clicker = 2;
                    }
                    else if (m.Y >= 101 && m.Y <= 150 && clicker != 3)
                    {
                        screenfill(55);
                        currentcolor = 55;
                        clicker = 3;
                    }
                    else if (m.Y >= 151 && clicker != 4) {
                        screenfill(4);
                        currentcolor = 4;
                        clicker = 4;
                    }
                }
            }

            if (m.Buttons == Mouse.MouseState.Right)
            {
                screenfill(7);
            }

                mouseX = m.X;
            mouseY = m.Y;
            if (mouseX != mouseX2 || mouseY != mouseY2)
            {

                screen.SetPixel((uint)mouseX2, (uint)mouseY2, (uint)currentcolor);
                screen.SetPixel((uint)mouseX2 + 1, (uint)mouseY2, (uint)currentcolor);
                screen.SetPixel((uint)mouseX2 + 2, (uint)mouseY2, (uint)currentcolor);
                screen.SetPixel((uint)mouseX2, (uint)mouseY2 + 1, (uint)currentcolor);
                screen.SetPixel((uint)mouseX2, (uint)mouseY2 + 2, (uint)currentcolor);
                screen.SetPixel((uint)mouseX2 + 1, (uint)mouseY2 + 1, (uint)currentcolor);
                screen.SetPixel((uint)mouseX2 + 2, (uint)mouseY2 + 2, (uint)currentcolor);
                screen.SetPixel((uint)mouseX2 + 3, (uint)mouseY2 + 3, (uint)currentcolor);

                if (mouseX2 <= 11)
                {
                    if (mouseY2 <= 50 && clicker != 1)
                    {
                        DrawFilledRectangle(0, 0, 10, 50, 3);
                        if (mouseY2 >= 46)
                        {
                            DrawFilledRectangle(0, 50, 10, 50, 2);
                        }
                    }
                    if (mouseY2 >= 51 && m.Y <= 100 && clicker != 2)
                    {
                        DrawFilledRectangle(0, 50, 10, 50, 2);
                        if (mouseY2 >= 96)
                        {
                            DrawFilledRectangle(0, 50, 10, 50, 2);
                        }
                    }
                    if (mouseY2 >= 101 && m.Y <= 150 && clicker != 3)
                    {
                        DrawFilledRectangle(0, 100, 10, 50, 55);
                        if (mouseY2 >= 146)
                        {
                            DrawFilledRectangle(0, 50, 10, 50, 2);
                        }
                    }
                   if (mouseY2 >= 151 && clicker != 4) {
                        DrawFilledRectangle(0, 150, 10, 50, 4);
                    }
                    DrawFilledRectangle(10, 0, 1, height, 100);
                }

                mouseX2 = mouseX;
                mouseY2 = mouseY;
                screen.SetPixel((uint)mouseX, (uint)mouseY, 40);
                screen.SetPixel((uint)mouseX + 1, (uint)mouseY, 40);
                screen.SetPixel((uint)mouseX + 2, (uint)mouseY, 40);
                screen.SetPixel((uint)mouseX, (uint)mouseY + 1, 40);
                screen.SetPixel((uint)mouseX, (uint)mouseY + 2, 40);
                screen.SetPixel((uint)mouseX + 1, (uint)mouseY + 1, 40);
                screen.SetPixel((uint)mouseX + 2, (uint)mouseY + 2, 40);
                screen.SetPixel((uint)mouseX + 3, (uint)mouseY + 3, 40);


            }

        }

        public virtual void setPixel(int x, int y, int c)
        {
            if (screen.GetPixel320x200x8((uint)x, (uint)y) != (uint)c)
                setPixelRaw(x, y, c);
        }

        public virtual void clear(int c)
        {
            screen.Clear(c);
        }

        public void setPixelRaw(int x, int y, int c)
        {
            screen.SetPixel320x200x8((uint)x, (uint)y, (uint)c);
        }

        public void DrawFilledRectangle(uint x0, uint y0, int Width, int Height, int color)
        {
            for (uint i = 0; i < Width; i++)
            {
                for (uint h = 0; h < Height; h++)
                {
                    setPixel((int)(x0 + i), (int)(y0 + h), color);
                }
            }
        }
    }
}